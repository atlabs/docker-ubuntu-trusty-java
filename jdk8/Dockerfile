# vim:set ft=dockerfile:
FROM ubuntu:trusty

# grab gosu for easy step-down from root
ENV GOSU_VERSION 1.9
RUN set -x \
	&& apt-get update && apt-get install -y --no-install-recommends ca-certificates wget equivs unzip dnsutils && rm -rf /var/lib/apt/lists/* \
	&& wget -O /usr/local/bin/gosu "https://github.com/tianon/gosu/releases/download/$GOSU_VERSION/gosu-$(dpkg --print-architecture)" \
	&& wget -O /usr/local/bin/gosu.asc "https://github.com/tianon/gosu/releases/download/$GOSU_VERSION/gosu-$(dpkg --print-architecture).asc" \
	&& export GNUPGHOME="$(mktemp -d)" \
	&& gpg --keyserver ha.pool.sks-keyservers.net --recv-keys B42F6819007F00F88E364FD4036A9C25BF357DD4 \
	&& gpg --batch --verify /usr/local/bin/gosu.asc /usr/local/bin/gosu \
	&& rm -r "$GNUPGHOME" /usr/local/bin/gosu.asc \
	&& chmod +x /usr/local/bin/gosu \
	&& gosu nobody true \
	&& apt-get purge -y --auto-remove ca-certificates

# Default to UTF-8 file.encoding
ENV LANG="C.UTF-8"

ENV JAVA_VERSION="8u111"
ENV JAVA_BUILD="b14"
ENV JAVA_DIR="jdk1.8.0_111"
ENV JAVA_HOME="/usr/lib/jvm/${JAVA_DIR}"

RUN wget -nv --no-check-certificate --no-cookies \
    --header "Cookie: oraclelicense=accept-securebackup-cookie" \
    http://download.oracle.com/otn-pub/java/jdk/$JAVA_VERSION-$JAVA_BUILD/jdk-$JAVA_VERSION-linux-x64.tar.gz && \
    mkdir -p /usr/lib/jvm && \
    tar zxvf jdk-$JAVA_VERSION-linux-x64.tar.gz -C /usr/lib/jvm && \
    rm jdk-$JAVA_VERSION-linux-x64.tar.gz && \
    update-alternatives --install "/usr/bin/java" "java" "${JAVA_HOME}/bin/java" 1 && \
    update-alternatives --set java ${JAVA_HOME}/bin/java

COPY dummy-oracle-jdk .
RUN equivs-build dummy-oracle-jdk && \
    dpkg -i dummy-oracle-jdk_1.8_all.deb && \
    rm dummy-oracle-jdk* && \
    apt-get purge -y --auto-remove equivs

CMD ["java", "-version"]
