#!/usr/bin/env bash
# Usage: ./calculate-heap-limit.sh 0.75
# This will look at the maximum available memory in the container, and apply a
# ratio of 75%, returning a string value that you can use for your jvm "-Xmx"
# setting. Note that if no memory restriction is set, the string "default" is
# returned.

: ${JVM_RATIO:=0.75}
if [[ ! -z "$1" ]]; then
	JVM_RATIO=$1
fi

HOST_MEM=$(($(awk '/MemTotal/ {print $2}' /proc/meminfo)*1024)) # host memory in bytes
CONTAINER_MEM=$(cat /sys/fs/cgroup/memory/memory.limit_in_bytes) # will return 18446744073709551615 when not set

# If the container memory is restricted, apply the heap ratio limit
if (( ${CONTAINER_MEM} <= ${HOST_MEM} )); then
	 MEM="$(awk '{printf("%d",$1*$2/1024^2)}' <<<" ${CONTAINER_MEM} ${JVM_RATIO} ")m"
else
	 MEM="default"
fi
echo ${MEM}
