# ubuntu-trusty-java

A docker image with Ubuntu 14.04 and Oracle JDK.

# Building and Pushing

Create this image as follows:

        sudo docker build -t atlp/ubuntu-trusty-java:jre8 jre8/.

Now that our image is built, push it to your registry:

        sudo docker push atlp/ubuntu-trusty-java:jre8

